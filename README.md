# OpenJDK Early Access Docker Image with Maven

## Prior Work
This Dockerfile is based on the following projects:
- [Update to Java 11 now!](http://dev.solita.fi/2018/03/02/Update-to-Java11.html)
- [Docker Maven](https://github.com/carlossg/docker-maven)

## Contents
- [Centos 7 Base Image](https://hub.docker.com/r/library/centos/tags/centos7/)
- [Open JDK Project Amber](http://openjdk.java.net/projects/amber/) build from one of the available branches eg patterns or raw-string-literals
- [Maven 3.5.3](http://maven.apache.org/ref/3.5.3/)

## Usage

After building an OpenJDK branch using the following commands:
```bash
hg clone http://hg.openjdk.java.net/amber/amber
hg update patterns
cd amber
sh configure && make images
./build/*/images/jdk/bin/java -version
```

Then to build with a tag for Docker Hub where the JDK_DIR is the relative path of the OpenJDK build.

```bash
docker build -t hub.docker.com/<your-docker-hub>/openjdk-project-amber-raw-string-literals --build-arg JDK_DIR=raw-string-literals/jdk .
```

Then to run the image would be:
```bash
 docker run --rm  hub.docker.com/<your-docker-hub>/openjdk-project-amber-raw-string-literals
```

The output should be something like:
```bash
Apache Maven 3.5.3 (3383c37e1f9e9b3bc3df5050c29c8aff9f295297; 2018-02-24T19:49:05Z)
Maven home: /usr/share/maven
Java version: 11-internal, vendor: Oracle Corporation
Java home: /opt/jdk-11
Default locale: en_US, platform encoding: ANSI_X3.4-1968
OS name: "linux", version: "4.13.16-041316-generic", arch: "amd64", family: "unix"
```

## Issues

Feedback or issues are most welcome, please use the issues link [here](https://gitlab.com/codenomads/docker-openjdk-project-amber/issues).
